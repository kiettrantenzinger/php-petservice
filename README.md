# A simple Tenzinger Microservice setup
A simple Tenzinger microservice setup with the possiblity to create API endpoints.

### Requirements
- [Docker](https://www.docker.com)

### Installation
Follow the steps to install this project on your local machine.

```bash
$ git clone git@bitbucket.org:kiettrantenzinger/php-petservice.git
```

#### Set up Traefik to manage all localhost traffic to each project.
Create a public network.
```bash
$ make network
```

Run traefik.
```bash
$ make traefik
```

#### Run Docker.
```bash
$ make up
```

#### Connect to the database.
```bash
Host: 127.0.0.1
Port: 3306
Name: database
User: root
Pass: <empty>
```

#### Run migrations.
```bash
$ make migrate
```

#### Run fixtures.
```bash
$ make fixture
```

#### Run tests.
##### Test without code coverage.
```bash
$ make test
```
##### Test with code coverage.
```bash
$ make test-with-coverage
```
