HELL := /bin/bash

.DEFAULT_GOAL := help
.PHONY: all $(MAKECMDGOALS) #see https://stackoverflow.com/questions/44492805/makefile-declare-all-targets-phony

.RUN =
.DOCKER_COMPOSE := docker-compose
.DOCKER_COMPOSE_RUN := ${.DOCKER_COMPOSE} run --rm

ifneq ($(AM_I_INSIDE_DOCKER),true)
    .RUN := $(.DOCKER_COMPOSE_RUN)
endif

.PHONY: all $(MAKECMDGOALS) #see https://stackoverflow.com/questions/44492805/makefile-declare-all-targets-phony

help: ## Display this help.
	@printf "\nUsage:\n  make \033[36m<target>\033[0m"
	@awk 'BEGIN {FS = ":.*##"; printf "\033[36m\033[0m\n\nTargets:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-24s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)
.PHONY: help

network: ## Create a public network.
	docker network create public
.PHONY: public-network

traefik: ## Run traefik network.
	docker run -d --rm --name router -v /var/run/docker.sock:/var/run/docker.sock --network public -p 80:80 -p 8080:8080 traefik:1.7-alpine --api --docker --docker.exposedbydefault=false
.PHONY: network

build: ## Build the containers
	${.DOCKER_COMPOSE} build --pull
.PHONY: build

install: ## Install the dependencies with Composer.
	${.DOCKER_COMPOSE_RUN} composer install --prefer-dist --no-progress
.PHONY: install

up: ## Run the containers.
	${.DOCKER_COMPOSE} up
.PHONY: containers

down: ## Stop the containers.
	${.DOCKER_COMPOSE} down
.PHONY: stop-containers

migrate: ## Run the doctrine migrations.
	${.DOCKER_COMPOSE_RUN} php /bin/bash -c "./bin/console doctrine:migrations:migrate --no-interaction --allow-no-migration"
.PHONY: migrations

test: ## Run the unit tests.
	${.RUN} php /bin/bash -c "./bin/console doctrine:database:create --env=test --if-not-exists && ./bin/console doctrine:migrations:migrate --env=test --no-interaction --allow-no-migration && ./vendor/bin/simple-phpunit --testdox"
.PHONY: tests

test-with-coverage: ## Run the unit tests with XML coverage report.
	${.RUN} php /bin/bash -c "./bin/console doctrine:database:create --env=test --if-not-exists && ./bin/console doctrine:migrations:migrate --env=test --no-interaction --allow-no-migration && ./vendor/bin/simple-phpunit --testdox --coverage-html=coverage"
.PHONY: tests-with-coverage

code-style-fix: ## Fix the code style.
	${.RUN} php ./vendor/bin/php-cs-fixer fix --allow-risky=yes
.PHONY: code-style

code-style-check: ## Check the code style.
	${.RUN} php ./vendor/bin/php-cs-fixer fix --allow-risky=yes --dry-run -v
.PHONY: code-style-check

validate-composer: ## Validates the Composer configuration and lockfile.
	$(.RUN) composer validate --strict --ansi
.PHONY: validate-composer
