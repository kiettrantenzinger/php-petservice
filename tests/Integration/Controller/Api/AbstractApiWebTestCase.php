<?php

declare(strict_types=1);

namespace App\Tests\Integration\Controller\Api;

use App\Tests\TestClock;
use App\Tests\UuidMockFactory;
use App\Time\ClockInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidFactory;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpKernel\HttpKernelBrowser;

abstract class AbstractApiWebTestCase extends WebTestCase
{
    protected HttpKernelBrowser $client;

    protected TestClock $clock;

    protected UuidMockFactory $uuidMockFactory;

    protected function setUp(): void
    {
        parent::setUp();

        $this->uuidMockFactory = new UuidMockFactory();
        Uuid::setFactory($this->uuidMockFactory);

        $this->client = static::createClient();
        $this->clock = new TestClock();

        $container = $this->client->getContainer();
        $container->set(ClockInterface::class, $this->clock);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->client->disableReboot();

        Uuid::setFactory(new UuidFactory());
    }
}
