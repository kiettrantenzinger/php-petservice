<?php

declare(strict_types=1);

namespace App\Tests\Integration\Controller\Api\Pet;

use App\Tests\Integration\Controller\Api\AbstractApiWebTestCase;
use App\Tests\PetFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

final class UpdateControllerTest extends AbstractApiWebTestCase
{
    public function testCanUpdatePet(): void
    {
        $this->uuidMockFactory->setUuid4('1260b44d-5a65-4c0d-8a55-03d5e7376b2f');
        $this->clock->fixate('2021-01-01 13:33:37');

        $entityManager = $this->getContainer()->get(EntityManagerInterface::class);
        $petFactory = new PetFactory($entityManager);

        $pet = $petFactory->createPet(
            'Pitbull',
            'fe6f3325-db3d-4e32-8f2f-5a5bc5dfec4f'
        );

        $this->client->request(
            'PATCH',
            sprintf('/api/pets/%s', $pet->getUuid()->toString()),
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            file_get_contents(__DIR__.'/../../../Resources/Api/Pet/UpdateControllerRequest.json')
        );

        $response = $this->client->getResponse();

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertJsonStringEqualsJsonFile(
            __DIR__.'/../../../Resources/Api/Pet/UpdateControllerResponse.json',
            $response->getContent()
        );
    }
}
