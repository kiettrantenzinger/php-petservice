<?php

declare(strict_types=1);

namespace App\Tests\Integration\Controller\Api\Pet;

use App\Tests\Integration\Controller\Api\AbstractApiWebTestCase;
use Symfony\Component\HttpFoundation\Response;

final class CreateControllerTest extends AbstractApiWebTestCase
{
    public function testCanCreatePet(): void
    {
        $this->uuidMockFactory->setUuid4('1260b44d-5a65-4c0d-8a55-03d5e7376b2f');
        $this->clock->fixate('2021-01-01 13:33:37');

        $this->client->request(
            'POST',
            '/api/pets',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            file_get_contents(__DIR__.'/../../../Resources/Api/Pet/CreateControllerRequest.json')
        );

        $response = $this->client->getResponse();

        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);
        $this->assertJsonStringEqualsJsonFile(
            __DIR__.'/../../../Resources/Api/Pet/CreateControllerResponse.json',
            $response->getContent()
        );
    }
}
