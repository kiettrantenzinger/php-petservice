<?php

declare(strict_types=1);

namespace App\Tests;

use App\Entity\Pet;
use Doctrine\ORM\EntityManagerInterface;

final class PetFactory
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function createPet(string $name, string $shelterReference): Pet
    {
        $pet = new Pet();
        $pet->setName($name);
        $pet->setShelterReference($shelterReference);

        $this->entityManager->persist($pet);
        $this->entityManager->flush();

        return $pet;
    }
}
