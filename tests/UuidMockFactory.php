<?php

declare(strict_types=1);

namespace App\Tests;

use Ramsey\Uuid\UuidFactory;
use Ramsey\Uuid\UuidInterface;

final class UuidMockFactory extends UuidFactory
{
    private ?string $uuid4;

    public function __construct(?string $uuid4 = null)
    {
        parent::__construct();

        $this->uuid4 = $uuid4;
    }

    public function reset(): void
    {
        $this->uuid4 = null;
    }

    public function setUuid4(?string $uuid4): void
    {
        $this->uuid4 = $uuid4;
    }

    public function uuid4(): UuidInterface
    {
        if (null !== $this->uuid4) {
            return $this->fromString($this->uuid4);
        }

        return parent::uuid4();
    }
}
