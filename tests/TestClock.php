<?php

declare(strict_types=1);

namespace App\Tests;

use App\Time\ClockInterface;
use DateInterval;
use DateTimeImmutable;
use DateTimeZone;
use InvalidArgumentException;

final class TestClock implements ClockInterface
{
    private DateTimeImmutable $time;
    private DateTimeZone $timeZone;

    public function __construct(DateTimeZone $timeZone = null)
    {
        $this->timeZone = $timeZone ?: new DateTimeZone('UTC');
        $this->tick();
    }

    public function tick(): void
    {
        $this->time = new DateTimeImmutable('now', $this->timeZone);
    }

    public function fixate(string $input): void
    {
        $preciseTime = sprintf('%s.000000', $input);
        $dateTime = DateTimeImmutable::createFromFormat('Y-m-d H:i:s.u', $preciseTime, $this->timeZone);

        if (false === $dateTime instanceof DateTimeImmutable) {
            throw new InvalidArgumentException("Invalid input for date/time fixation provided: {$input}");
        }

        $this->time = $dateTime;
    }

    public function moveForward(DateInterval $interval): void
    {
        $this->time = $this->now()->add($interval);
    }

    public function now(): DateTimeImmutable
    {
        return $this->time;
    }

    public function timeZone(): DateTimeZone
    {
        return $this->timeZone;
    }
}
