<?php

declare(strict_types=1);

namespace App\Tests\Unit\EventSubscriber;

use App\Entity\Pet;
use App\EventSubscriber\TimestampableSubscriber;
use App\Tests\TestClock;
use App\Time\ClockInterface;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use PHPUnit\Framework\TestCase;
use StdClass;

final class TimestampableSubscriberTest extends TestCase
{
    private TimestampableSubscriber $subscriber;

    private ClockInterface $clock;

    private EntityManagerInterface $entityManagerMock;

    protected function setUp(): void
    {
        $this->clock = new TestClock();
        $this->subscriber = new TimestampableSubscriber($this->clock);

        $this->entityManagerMock = $this->createMock(EntityManagerInterface::class);
    }

    public function testCanGetSubscribedEvents(): void
    {
        $this->assertSame(
            [
                'prePersist',
                'preUpdate',
            ],
            $this->subscriber->getSubscribedEvents()
        );
    }

    public function testCanPrePersist(): void
    {
        $this->clock->fixate('2021-01-01 13:33:37');

        $pet = new Pet();
        $lifecycleEventArgs = new LifecycleEventArgs($pet, $this->entityManagerMock);

        $this->subscriber->prePersist($lifecycleEventArgs);

        $createdAt = new DateTimeImmutable('2021-01-01 13:33:37');
        $this->assertEquals($createdAt, $pet->getCreatedAt());
    }

    public function testCannotPrePersist(): void
    {
        $this->clock->fixate('2021-01-01 13:33:37');

        $pet = $this->getMockBuilder(StdClass::class)->addMethods(['getCreatedAt'])->getMock();
        $lifecycleEventArgs = new LifecycleEventArgs($pet, $this->entityManagerMock);

        $this->subscriber->prePersist($lifecycleEventArgs);

        $pet->expects($this->never())->method('getCreatedAt');
    }

    public function testCanPreUpdate(): void
    {
        $this->clock->fixate('2021-01-01 13:33:37');

        $pet = new Pet();
        $lifecycleEventArgs = new LifecycleEventArgs($pet, $this->entityManagerMock);

        $this->subscriber->preUpdate($lifecycleEventArgs);

        $updatedAt = new DateTimeImmutable('2021-01-01 13:33:37');
        $this->assertEquals($updatedAt, $pet->getUpdatedAt());
    }

    public function testCannotPreUpdate(): void
    {
        $this->clock->fixate('2021-01-01 13:33:37');

        $pet = $this->getMockBuilder(StdClass::class)->addMethods(['getUpdatedAt'])->getMock();

        $lifecycleEventArgs = new LifecycleEventArgs($pet, $this->entityManagerMock);

        $this->subscriber->preUpdate($lifecycleEventArgs);

        $pet->expects($this->never())->method('getUpdatedAt');
    }
}
