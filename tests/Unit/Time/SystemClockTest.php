<?php

declare(strict_types=1);

namespace App\Tests\Unit\Time;

use App\Time\SystemClock;
use DateTimeImmutable;
use DateTimeZone;
use PHPUnit\Framework\TestCase;

final class SystemClockTest extends TestCase
{
    public function testCanGetNow(): void
    {
        $this->assertInstanceOf(DateTimeImmutable::class, (new SystemClock())->now());
    }

    public function testCanGetTimezone(): void
    {
        $this->assertInstanceOf(DateTimeZone::class, (new SystemClock())->timeZone());
    }
}
