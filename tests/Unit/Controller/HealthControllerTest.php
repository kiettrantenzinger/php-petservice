<?php

declare(strict_types=1);

namespace App\Tests\Unit\Controller;

use App\Controller\HealthController;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;

final class HealthControllerTest extends TestCase
{
    public function testCanReturnStatus200(): void
    {
        $controller = new HealthController();
        $response = $controller();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals('👍', $response->getContent());
    }
}
