<?php

declare(strict_types=1);

namespace App\Time;

use DateTimeImmutable;
use DateTimeZone;

interface ClockInterface
{
    public function now(): DateTimeImmutable;

    public function timeZone(): DateTimeZone;
}
