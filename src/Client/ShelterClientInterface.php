<?php

declare(strict_types=1);

namespace App\Client;

use App\Model\Shelter;

interface ShelterClientInterface
{
    public function getShelter(string $uuid): Shelter;
}
