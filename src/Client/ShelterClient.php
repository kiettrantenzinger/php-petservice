<?php

declare(strict_types=1);

namespace App\Client;

use App\Model\Shelter;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class ShelterClient implements ShelterClientInterface
{
    private SerializerInterface $serializer;

    private HttpClientInterface $httpClient;

    private string $baseUri;

    public function __construct(HttpClientInterface $httpClient, SerializerInterface $serializer, string $baseUri)
    {
        $this->serializer = $serializer;
        $this->httpClient = $httpClient;
        $this->baseUri = $baseUri;
    }

    public function getShelter(string $uuid): Shelter
    {
        $response = $this->httpClient->request(
            'GET',
            sprintf('/api/shelters/%s', $uuid),
            [
                'base_uri' => $this->baseUri,
            ]
        );

        return $this->serializer->deserialize($response->getContent(), Shelter::class, 'json');
    }
}
