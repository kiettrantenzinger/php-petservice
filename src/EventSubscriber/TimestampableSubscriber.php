<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Entity\TimestampableInterface;
use App\Time\ClockInterface;
use Doctrine\Bundle\DoctrineBundle\EventSubscriber\EventSubscriberInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

final class TimestampableSubscriber implements EventSubscriberInterface
{
    private ClockInterface $clock;

    public function __construct(ClockInterface $clock)
    {
        $this->clock = $clock;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
            Events::preUpdate,
        ];
    }

    public function prePersist(LifecycleEventArgs $lifecycleEventArgs): void
    {
        $object = $lifecycleEventArgs->getObject();
        if (false === $object instanceof TimestampableInterface) {
            return;
        }

        $object->setCreatedAt($this->clock->now());
    }

    public function preUpdate(LifecycleEventArgs $lifecycleEventArgs): void
    {
        $object = $lifecycleEventArgs->getObject();
        if (false === $object instanceof TimestampableInterface) {
            return;
        }

        $object->setUpdatedAt($this->clock->now());
    }
}
