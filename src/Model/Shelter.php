<?php

declare(strict_types=1);

namespace App\Model;

use DateTimeInterface;
use Ramsey\Uuid\UuidInterface;

class Shelter
{
    private UuidInterface $uuid;

    private string $name;

    private DateTimeInterface $createdAt;

    private ?DateTimeInterface $updatedAt;

    public function __construct(
        UuidInterface $uuid,
        string $name,
        DateTimeInterface $createdAt,
        ?DateTimeInterface $updatedAt = null
    ) {
        $this->uuid = $uuid;
        $this->name = $name;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }
}
