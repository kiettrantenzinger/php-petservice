<?php

declare(strict_types=1);

namespace App\Controller\Api;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

abstract class AbstractApiController
{
    protected SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    protected function createJsonResponse($data, int $statusCode = Response::HTTP_OK): Response
    {
        $response = new Response();
        $response->setContent(
            $this->serializer->serialize(
                $data,
                'json'
            )
        );
        $response->setStatusCode($statusCode);

        return $response;
    }
}
