<?php

declare(strict_types=1);

namespace App\Controller\Api\Pet;

use App\Controller\Api\AbstractApiController;
use App\Entity\Pet;
use Symfony\Component\HttpFoundation\Response;

final class GetController extends AbstractApiController
{
    public function __invoke(Pet $pet): Response
    {
        return $this->createJsonResponse($pet);
    }
}
