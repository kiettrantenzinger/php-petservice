<?php

declare(strict_types=1);

namespace App\Controller\Api\Pet;

use App\Controller\Api\AbstractApiController;
use App\Entity\Pet;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

final class UpdateController extends AbstractApiController
{
    private EntityManagerInterface $entityManager;

    public function __construct(SerializerInterface $serializer, EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        parent::__construct($serializer);
    }

    public function __invoke(Pet $pet, Request $request): Response
    {
        $this->serializer->deserialize(
            $request->getContent(),
            Pet::class,
            'json',
            [
                'object_to_populate' => $pet,
            ]
        );

        $this->entityManager->flush();

        return $this->createJsonResponse($pet);
    }
}
