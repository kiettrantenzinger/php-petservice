<?php

declare(strict_types=1);

namespace App\Controller\Api\Pet;

use App\Controller\Api\AbstractApiController;
use App\Repository\PetRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

final class GetListController extends AbstractApiController
{
    private PetRepository $petRepository;

    public function __construct(SerializerInterface $serializer, PetRepository $petRepository)
    {
        $this->petRepository = $petRepository;

        parent::__construct($serializer);
    }

    public function __invoke(): Response
    {
        $pets = $this->petRepository->findAll();

        return $this->createJsonResponse($pets);
    }
}
