<?php

declare(strict_types=1);

namespace App\Controller\Api\Pet;

use App\Controller\Api\AbstractApiController;
use App\Entity\Pet;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

final class DeleteController extends AbstractApiController
{
    private EntityManagerInterface $entityManager;

    public function __construct(SerializerInterface $serializer, EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        parent::__construct($serializer);
    }

    public function __invoke(Pet $pet): Response
    {
        $this->entityManager->remove($pet);
        $this->entityManager->flush();

        return $this->createJsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
