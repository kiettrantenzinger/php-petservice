<?php

declare(strict_types=1);

namespace App\Controller\Api\Pet;

use App\Client\ShelterClientInterface;
use App\Controller\Api\AbstractApiController;
use App\Entity\Pet;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

final class CreateController extends AbstractApiController
{
    private EntityManagerInterface $entityManager;

    private ShelterClientInterface $shelterClient;

    public function __construct(
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager,
        ShelterClientInterface $shelterClient
    ) {
        $this->entityManager = $entityManager;
        $this->shelterClient = $shelterClient;

        parent::__construct($serializer);
    }

    public function __invoke(Request $request): Response
    {
        /** @var Pet $pet */
        $pet = $this->serializer->deserialize(
            $request->getContent(),
            Pet::class,
            'json'
        );

        /*
         * Check data if exists.
         * $this->shelterClient->getShelter($pet->getShelterReference());
         */

        $this->entityManager->persist($pet);
        $this->entityManager->flush();

        return $this->createJsonResponse($pet, Response::HTTP_CREATED);
    }
}
