openapi: 3.0.3
info:
  title: Pet
  version: 1.0.0
  description: |
    # Documentation changelog
    This API is for creating, retrieving and viewing a pet.
paths:
  /pets:
    post:
      summary: Create a pet
      description: API endpoint for creating a pet.
      operationId: createPet
      x-symfony-controller: 'App\Controller\Api\Pet\CreateController'
      responses:
        '201':
          description: The pet has been successfully created.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Pet'
        '400':
          $ref: '#/components/responses/BadJsonRequestError'
        '422':
          $ref: '#/components/responses/InvalidRequestError'
        default:
          $ref: '#/components/responses/UnexpectedError'
      tags:
        - Pet
    get:
      summary: Get a list of pets
      description: API endpoint for getting a list of pets.
      operationId: getPets
      x-symfony-controller: 'App\Controller\Api\Pet\GetListController'
      responses:
        '200':
          description: The pets has been successfully retrieved.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Pet'
        default:
          $ref: '#/components/responses/UnexpectedError'
      tags:
        - Pet
  /pets/{uuid}:
    patch:
      summary: Update a pet
      description: API endpoint for updating a pet.
      operationId: updatePet
      x-symfony-controller: 'App\Controller\Api\Pet\UpdateController'
      responses:
        '201':
          description: The pet has been successfully updated.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Pet'
        '400':
          $ref: '#/components/responses/BadJsonRequestError'
        '404':
          description: The pet with the provided UUID is not found.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '422':
          $ref: '#/components/responses/InvalidRequestError'
        default:
          $ref: '#/components/responses/UnexpectedError'
      tags:
        - Pet
    delete:
      summary: Delete a pet
      description: API endpoint for deleting a pet.
      operationId: deletePet
      x-symfony-controller: 'App\Controller\Api\Pet\DeleteController'
      responses:
        '204':
          description: The pet has been successfully deleted.
        '404':
          description: The pet with the provided UUID is not found.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '422':
          $ref: '#/components/responses/InvalidRequestError'
        default:
          $ref: '#/components/responses/UnexpectedError'
      tags:
        - Pet
    get:
      summary: View a pet
      description: API endpoint for viewing a pet.
      operationId: getPet
      x-symfony-controller: 'App\Controller\Api\Pet\GetController'
      responses:
        '200':
          description: The pets has been successfully retrieved.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Pet'
        default:
          $ref: '#/components/responses/UnexpectedError'
      tags:
        - Pet
    parameters:
      - $ref: '#/components/parameters/uuid'
components:
  schemas:
    Pet:
      type: object
      properties:
        uuid:
          type: string
          format: uuid
          example: 195ce20e-e7e3-4c8f-a9b1-595fc7d0118d
          readOnly: true
        name:
          type: string
          description: The name of the pet.
          maxLength: 255
          example: 'Pitbull'
        shelterReference:
          type: string
          description: An external reference to a shelter.
          maxLength: 255
          example: '2b1531e2-7877-43c4-be3a-1f52e1cde630'
        createdAt:
          type: string
          format: date-time
        updatedAt:
          type: string
          format: date-time
      required:
        - name
        - shelterReference
    Error:
      type: object
      properties:
        message:
          type: string
      required:
        - message
    InvalidRequestError:
      allOf:
        - $ref: '#/components/schemas/Error'
        - type: object
          properties:
            errors:
              type: array
              items:
                type: string
  responses:
    BadJsonRequestError:
      description: The request body is not valid JSON.
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/InvalidRequestError'
    InvalidRequestError:
      description: A JSON validation request error.
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/InvalidRequestError'
    ResourceNotFoundError:
      description: The resource cannot be found.
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'
    UnexpectedError:
      description: An unexpected error has occurred. Try again or contact us when the error persists.
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'
  parameters:
    uuid:
      name: uuid
      description: The unique identifier of the order.
      in: path
      required: true
      schema:
        type: string
        format: uuid
        example: 180f1eb7-3001-4620-b94e-5199a13aa03e
tags:
  - name: Pet
    description: Endpoints for creating, reading, updating and deleting pets.
